SCRIPTS=$(wildcard */)

.PHONY=all

%/: FORCE
	make -C $@ install

all: $(SCRIPTS)
	make $(SCRIPTS)

FORCE:
