# Collection of scripts

clone recursively:

```bash
git clone --recurse-submodules -j8 https://gitlab.com/cbo77-scripts
```

(-j8 for up to 8 simultaneous processes)
